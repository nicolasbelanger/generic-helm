NOTE: It may take a few minutes for the LoadBalancer IP to be available.
       You can watch the status of by running 'kubectl get --namespace {{ .Release.Namespace }} svc -w {{ include "generic-helm.fullname" . }}'
export SERVICE_IP=$(kubectl get svc --namespace {{ .Release.Namespace }} {{ include "generic-helm.fullname" . }} -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo http://$SERVICE_IP

.PHONY: package
package:
	cd packages && \
	helm package .. && \
	helm repo index .

.PHONY: release
release:
	git add Chart.yaml packages
	git commit -m 'New chart version'
	git push

.PHONY: all
all: package release